package com.bot.rain.rainbot.Activity;

import com.journeyapps.barcodescanner.CaptureActivity;

/**
 * This Activity is exactly the same as CaptureActivity, but has a different orientation
 * setting in AndroidManifest.xml.
 */
public class AnyOrientation extends CaptureActivity {

}
