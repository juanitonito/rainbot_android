package com.bot.rain.rainbot.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bot.rain.rainbot.Adapter.ProfilPlanteAdapter;
import com.bot.rain.rainbot.Model.Plante;
import com.bot.rain.rainbot.Model.ProfilPlante;
import com.bot.rain.rainbot.R;
import com.bot.rain.rainbot.Service.APIRestService;
import com.bot.rain.rainbot.Service.InitiatorAPI;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.DisconnectedBufferOptions;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailPlanteActivity extends AppCompatActivity implements InitiatorAPI {
    private Plante plante;
    private String idPlante;
    private List<ProfilPlante> profilsPlantes;
    private ProfilPlanteAdapter adapter;
    private Spinner spin;
    public SharedPreferences sharedPreferences;
    APIRestService api;
    EditText edHeader;
    public enum State {CREATION,EDITION};
    private State status;


    ///MQTT
    MqttAndroidClient mqttAndroidClient;
    String serverUri;

    String clientId = "ExampleAndroidClient";
    String subscriptionTopic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_plante);
        sharedPreferences = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        String IP = sharedPreferences.getString("IP","192.168.1.64");
        api = initAPIService(IP);
        Intent i = getIntent();
        serverUri = "tcp://"+IP+":1883";
        //Get parameters of intent
        idPlante = i.getStringExtra("ID_PLANTE");
        subscriptionTopic = "sensor/" + idPlante;
        // Pour s'abonner à tous les sensors
//        subscriptionTopic = "sensor/#";
        initAdapterSpinner();
        initQueue();
    }

    private void initQueue() {
        mqttAndroidClient = new MqttAndroidClient(getApplicationContext(), serverUri, clientId);
        mqttAndroidClient.setCallback(new MqttCallbackExtended() {
            @Override
            public void connectComplete(boolean reconnect, String serverURI) {

                if (reconnect) {
                    // Because Clean Session is true, we need to re-subscribe
                    subscribeToTopic();
                }
            }

            @Override
            public void connectionLost(Throwable cause) {

            }

            @Override
            public void messageArrived(String topic, MqttMessage message) throws Exception {
                onReceiveMessage(message);
            }

            @Override
            public void deliveryComplete(IMqttDeliveryToken token) {

            }
        });

        MqttConnectOptions mqttConnectOptions = new MqttConnectOptions();
        mqttConnectOptions.setAutomaticReconnect(true);
        mqttConnectOptions.setCleanSession(false);

        try {
            //addToHistory("Connecting to " + serverUri);
            mqttAndroidClient.connect(mqttConnectOptions, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    DisconnectedBufferOptions disconnectedBufferOptions = new DisconnectedBufferOptions();
                    disconnectedBufferOptions.setBufferEnabled(true);
                    disconnectedBufferOptions.setBufferSize(100);
                    disconnectedBufferOptions.setPersistBuffer(false);
                    disconnectedBufferOptions.setDeleteOldestMessages(false);
                    mqttAndroidClient.setBufferOpts(disconnectedBufferOptions);
                    Log.d("debug_mqtt", "connexion ok");
                    subscribeToTopic();
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {

                    Log.d("debug_mqtt", "error :" + exception.getMessage());
                }
            });


        } catch (MqttException ex){
            ex.printStackTrace();
        }

    }

    public void subscribeToTopic(){
        try {
            mqttAndroidClient.subscribe(subscriptionTopic, 0, null, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
//                    addToHistory("Subscribed!");
                    Log.d("debug_mqtt", "Subscribed");
                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
//                    addToHistory("Failed to subscribe");
                    Log.d("debug_mqtt", "Failed to subscribe");
                }
            });

            // THIS DOES NOT WORK!
            mqttAndroidClient.subscribe(subscriptionTopic, 0, new IMqttMessageListener() {
                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    onReceiveMessage(message);


                }
            });

        } catch (MqttException ex){
            System.err.println("Exception whilst subscribing");
            ex.printStackTrace();
        }
    }

    public void onReceiveMessage(MqttMessage message){
        // message Arrived!
        System.out.println("Message: " + " : " + new String(message.getPayload()));

//                    Log.d("debug_mqtt", "Message: " + topic + " : " + new String(message.getPayload()));
        String json = new String(message.getPayload());
        JSONObject reader = null;
        try {
            reader = new JSONObject(json);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("debug_mqtt", ""+ reader.toString());

        TextView lbHumidite = (TextView) findViewById(R.id.lbHumidite);
        try {
            lbHumidite.setText( reader.getString("humidity") +  " %");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    public void initAdapterSpinner(){

        //GET PLANTES FROM backend 
        //http://localhost:8080/ProfilsPlantes/

        Call<List<ProfilPlante>> call = api.getProfilsPlantes();
        call.enqueue(new Callback<List<ProfilPlante>>(){
            @Override
            public void onResponse(Call<List<ProfilPlante>> call, Response<List<ProfilPlante>> response) {
                profilsPlantes = response.body();

                // Initialize the adapter sending the current context
                // Send the simple_spinner_item layout
                // And finally send the ProfilPlantes array (Your data)
                adapter = new ProfilPlanteAdapter(DetailPlanteActivity.this,
                        android.R.layout.simple_spinner_item,
                        profilsPlantes);
                spin = (Spinner) findViewById(R.id.spDetailProfilPlante);
                spin.setAdapter(adapter); // Set the custom adapter to the spinner
                // You can create an anonymous listener to handle the event when is selected an spinner item

                init(idPlante);
                spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int position, long id) {
                        // Here you get the current item (a ProfilPlante object) that is selected by its position
                        ProfilPlante ProfilPlante = adapter.getItem(position);
                        // Here you can do the action you want to...
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {  }
                });

            }

            @Override
            public void onFailure(Call<List<ProfilPlante>> call, Throwable t) {
                //Toast.makeText(DetailScanActivity.this, "Error", Toast.LENGTH_LONG).show();
                Log.d("b_fail", t.getMessage());
            }
        });
    }


    public void init(final String idPlante){

        //GET PLANTES FROM backend
        //http://localhost:8080/ProfilsPlantes/

        Call<Plante> call = api.getDetailPlante(idPlante);
        call.enqueue(new Callback<Plante>(){
            @Override
            public void onResponse(Call<Plante> call, Response<Plante> response) {
                Plante p = response.body();

                //CREATE PLANTE OBJECT
                //Profil plante
                Spinner spProfilPlante = (Spinner) findViewById(R.id.spDetailProfilPlante);
                spProfilPlante.setSelection(adapter.getPosition(p.getProfil()));
//                spProfilPlante.
//                ProfilPlante profilPlante = (ProfilPlante) spProfilPlante.getSelectedItem();
//                profilsPlantes.get

                //ID PLANTE
                TextView lbIdBoitier = (TextView) findViewById(R.id.lbIdPlante);
                lbIdBoitier.setText(p.getId());


                //NOM PLANTE
                EditText edNomPlante = (EditText) findViewById(R.id.edDetailNomPlante);
                edNomPlante.setText(p.getName());

                //LATITUDE PLANTE
                EditText edLatitude = (EditText) findViewById(R.id.edDetailLatitude);
                edLatitude.setText(p.getLatitude()+"");


                //LONGITUDE PLANTE
                EditText edLongitude = (EditText) findViewById(R.id.edDetailLongitude);
                edLongitude.setText(""+p.getLongitude());
            }

            @Override
            public void onFailure(Call<Plante> call, Throwable t) {

            }
        });
    }


    @Override
    public APIRestService initAPIService(String ip) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://" + ip + ":8080")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        return retrofit.create(APIRestService.class);
    }
}
