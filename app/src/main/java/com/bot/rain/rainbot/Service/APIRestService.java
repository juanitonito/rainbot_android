package com.bot.rain.rainbot.Service;

import com.bot.rain.rainbot.Model.Plante;
import com.bot.rain.rainbot.Model.ProfilPlante;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface APIRestService {
    @Headers("Content-Type: application/json")
    @POST("/plante/")
    Call<Void> addPlante(@Body Plante plante);

    @GET("/profilsPlantes/")
    Call<List<ProfilPlante>> getProfilsPlantes();

    @GET("/plantes/")
    Call<List<Plante>> getPlantes();

    @GET("/plante/{planteId}")
    Call<Plante> getDetailPlante(@Path("planteId") String planteId);

}
