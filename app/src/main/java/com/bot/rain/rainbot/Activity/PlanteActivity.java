package com.bot.rain.rainbot.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.bot.rain.rainbot.Adapter.PlanteAdapter;
import com.bot.rain.rainbot.Model.Plante;
import com.bot.rain.rainbot.R;
import com.bot.rain.rainbot.Service.APIRestService;
import com.bot.rain.rainbot.Service.InitiatorAPI;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PlanteActivity extends AppCompatActivity implements InitiatorAPI{
    public APIRestService api;
    public SharedPreferences sharedPreferences;
    public String userID = "5d6a4bbb6616203b576fe45e";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plante);
        sharedPreferences = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        String IP = sharedPreferences.getString("IP","192.168.1.64");
        api = initAPIService(IP);
        showPlantes();
    }


    public void showPlantes(){
        Call<List<Plante>> call = api.getPlantes();
        call.enqueue(new Callback<List<Plante>>(){
            @Override
            public void onResponse(Call<List<Plante>> call, Response<List<Plante>> response) {
                List<Plante> plantes = response.body();
                PlanteAdapter adapter = new PlanteAdapter(PlanteActivity.this, plantes);

                ListView list_plantes = (ListView) findViewById(R.id.lvProfils);


                list_plantes.setAdapter(adapter);
                list_plantes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
                        Plante item_selected = (Plante) adapter.getItemAtPosition(position);
                        Intent intent = new Intent(PlanteActivity.this, DetailPlanteActivity.class);
                        intent.putExtra("ID_PLANTE", item_selected.getId());
                        intent.putExtra("STATE", false);
                        startActivity(intent);
                    }
                });
            }

            @Override
            public void onFailure(Call<List<Plante>> call, Throwable t) {
                //Toast.makeText(DetailScanActivity.this, "Error", Toast.LENGTH_LONG).show();
                Log.d("b_fail", t.getMessage());
            }
        });
    }

    @Override
    public APIRestService initAPIService(String ip) {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://"+ip+":8080")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        return retrofit.create(APIRestService.class);
    }
}
