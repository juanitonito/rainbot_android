package com.bot.rain.rainbot.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.bot.rain.rainbot.Model.Plante;
import com.bot.rain.rainbot.R;

import java.util.List;

public class PlanteAdapter extends ArrayAdapter<Plante> {
    private List<Plante> values;
    private Context context;

    public PlanteAdapter(Context context, List<Plante> values) {
        super(context, R.layout.row_plante, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        PlanteHolder holder = new PlanteHolder();
        if (row == null) {
            LayoutInflater inflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.row_plante, parent, false);
        }
        Plante plante = values.get(position);
        TextView nom = (TextView) row.findViewById(R.id.libNom);
        nom.setText(plante.getName());


//        holder.removePaymentButton = (ImageButton)row.findViewById(R.id.removeBtn);
//        holder.removePaymentButton.setTag(holder.ProfilActivity);

        holder.name = (TextView)row.findViewById(R.id.libNom);

        row.setTag(holder);

        return row;
    }

    private void setupItem(PlanteHolder holder) {
        holder.name.setText(holder.plante.getName());
    }

    public static class PlanteHolder {
        Plante plante;
        TextView name;
        ImageButton removePaymentButton;
    }
}
