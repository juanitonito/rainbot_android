package com.bot.rain.rainbot.Activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bot.rain.rainbot.Adapter.ProfilPlanteAdapter;
import com.bot.rain.rainbot.Model.Plante;
import com.bot.rain.rainbot.Model.ProfilPlante;
import com.bot.rain.rainbot.R;
import com.bot.rain.rainbot.Service.APIRestService;
import com.bot.rain.rainbot.Service.InitiatorAPI;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.io.InputStream;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DetailScanActivity extends AppCompatActivity implements InitiatorAPI {
    private ProfilPlanteAdapter adapter;
    private Spinner spin;
    private TextView mTextMessage;
    private IntentIntegrator integrator;
    public SharedPreferences sharedPreferences;

    public final int CUSTOMIZED_REQUEST_CODE = 0x0000ffff;
    APIRestService api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_scan);

        mTextMessage = (TextView) findViewById(R.id.message);
        sharedPreferences = getSharedPreferences("PREFS", Context.MODE_PRIVATE);
        String IP = sharedPreferences.getString("IP","192.168.1.64");
        api = initAPIService(IP);
        initAdapterSpinner();
//        api = initAPIService("10.0.2.2");

        IntentIntegrator integrator = new IntentIntegrator(DetailScanActivity.this);
        integrator.setCaptureActivity(AnyOrientation.class);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt("Scanner un QrCode");
        integrator.setOrientationLocked(false);
        integrator.setBeepEnabled(false);
        integrator.initiateScan();


        Button btnAddPlante = (Button) findViewById(R.id.btnAddPlante);
        btnAddPlante.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DetailScanActivity.this, "Ajout d'une plante en cours ...", Toast.LENGTH_SHORT).show();

                //CREATE PLANTE OBJECT
                //Profil plante
                Spinner spProfilPlante = (Spinner) findViewById(R.id.spProfilPlante);
                ProfilPlante profilPlante = (ProfilPlante) spProfilPlante.getSelectedItem();

                //ID PLANTE
                TextView lbIdBoitier = (TextView) findViewById(R.id.lbIdBoitier);
                String idPlante = (String) lbIdBoitier.getText();


                //NOM PLANTE
                EditText edNomPlante = (EditText) findViewById(R.id.edNomPlante);
                String nomPlante = (String) edNomPlante.getText().toString();

                //LATITUDE PLANTE
                EditText edLatitude = (EditText) findViewById(R.id.edLatitude);
                Float latPlante = (Float) Float.parseFloat(edLatitude.getText().toString());

                //LONGITUDE PLANTE
                EditText edLongitude = (EditText) findViewById(R.id.edLongitude);
                Float longPlante = (Float) Float.parseFloat(edLongitude.getText().toString());


                Log.d("DEBUG_CLICK", "onClick: "+spin.getSelectedItem().toString());

                Call<Void> addPlante = api.addPlante(new Plante(idPlante, nomPlante, profilPlante, latPlante, longPlante));
                addPlante.enqueue(new Callback<Void>() {
                    @Override
                    public void onResponse(Call<Void> call, Response<Void> response) {
                        Toast.makeText(getApplicationContext(),"Plante ajoutée", Toast.LENGTH_SHORT).show();
                        backToHome();
                    }

                    @Override
                    public void onFailure(Call<Void> call, Throwable t) {
                        Toast.makeText(getApplicationContext(),t.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }

    public void backToHome(){
//        Intent myIntent = new Intent(DetailScanActivity.this, HomeActivity.class);
//        DetailScanActivity.this.startActivity(myIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode != CUSTOMIZED_REQUEST_CODE && requestCode != IntentIntegrator.REQUEST_CODE) {
            // This is important, otherwise the result will not be passed to the fragment
            super.onActivityResult(requestCode, resultCode, data);
            return;
        }
        switch (requestCode) {
            case CUSTOMIZED_REQUEST_CODE: {
                //Toast.makeText(this, "REQUEST_CODE = " + requestCode, Toast.LENGTH_LONG).show();
                break;
            }
            default:
                break;
        }

        IntentResult result = IntentIntegrator.parseActivityResult(resultCode, data);

        if(result.getContents() == null) {
            Log.d("MainActivity", "Cancelled scan");
//            Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
            backToHome();
        } else {
            Log.d("MainActivity", "Scanned");
            //Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
        }
//        barcodeScan("3029330003533");
        barcodeScan(result.getContents());
    }
    public void barcodeScan(String s){
        TextView lbIdBoitier = (TextView) findViewById(R.id.lbIdBoitier);
        lbIdBoitier.setText(""+ s);

    }

    public APIRestService initAPIService(String ip){
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("http://"+ip+":8080")
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        return retrofit.create(APIRestService.class);
    }

    public void initAdapterSpinner(){

        //GET PLANTES FROM backend
        //http://localhost:8080/ProfilsPlantes/

        Call<List<ProfilPlante>> call = api.getProfilsPlantes();
        call.enqueue(new Callback<List<ProfilPlante>>(){
            @Override
            public void onResponse(Call<List<ProfilPlante>> call, Response<List<ProfilPlante>> response) {
                List<ProfilPlante> profilsPlantes = response.body();

                // Initialize the adapter sending the current context
                // Send the simple_spinner_item layout
                // And finally send the ProfilPlantes array (Your data)
                adapter = new ProfilPlanteAdapter(DetailScanActivity.this,
                        android.R.layout.simple_spinner_item,
                        profilsPlantes);
                spin = (Spinner) findViewById(R.id.spProfilPlante);
                spin.setAdapter(adapter); // Set the custom adapter to the spinner
                // You can create an anonymous listener to handle the event when is selected an spinner item
                spin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view,
                                               int position, long id) {
                        // Here you get the current item (a ProfilPlante object) that is selected by its position
                        ProfilPlante ProfilPlante = adapter.getItem(position);
                        // Here you can do the action you want to...
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> adapter) {  }
                });

            }

            @Override
            public void onFailure(Call<List<ProfilPlante>> call, Throwable t) {
                //Toast.makeText(DetailScanActivity.this, "Error", Toast.LENGTH_LONG).show();
                Log.d("b_fail", t.getMessage());
            }
        });
    }


    /* Classe de téléchargement d'une image */
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}


